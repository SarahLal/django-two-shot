from django.urls import path
from receipts.views import receipts_list_view

urlpatterns = [
    path("", receipts_list_view, name="home"),
]
