from django.shortcuts import render, get_list_or_404
from receipts.models import ExpenseCategory
from receipts.models import Account
from receipts.models import Receipt


# Create your views here.
def receipts_list_view(request):
    receipts_list = Receipt.objects.all()
    context = {
        "receipts": receipts_list,
    }
    return render(request, "receipt_list.html", context)
